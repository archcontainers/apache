# Changelog

* 2020.05.02.1 : Updated to 2020.05.02.1 - Fix tag to match org.label-schema.version string "2020.05.02.1"
* 2020.05.02.0 : Updated to 2020.05.02.0 - Now depends on quay.io/repository/archcontainers/archibald:v2020.05.02.0
